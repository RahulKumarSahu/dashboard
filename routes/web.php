<?php

use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\MultipicController;
use App\Http\Controllers\SliderController;
use App\Models\Brand;
use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');

Route::get('/', function () {
    $brands = Brand::all();
    return view('home', compact('brands'));
});

//Category Controller

Route::get('all', [CategoryController::class, 'index'])->name('all.category');
Route::post('add', [CategoryController::class, 'store'])->name('store.category');
Route::get('editcate/{id}', [CategoryController::class, 'editcate']);
Route::post('update/{id}', [CategoryController::class, 'update']);
Route::get('softdelete/{id}', [CategoryController::class, 'softdelete']);
Route::get('restore/{id}', [CategoryController::class, 'restore']);
Route::get('pdelete/{id}', [CategoryController::class, 'pdelete']);
//brands

Route::get('add/brand', [BrandController::class, 'index'])->name('brands');
Route::post('store/brand', [BrandController::class, 'store'])->name('store.brand');
Route::get('edit_brand/{id}', [BrandController::class, 'edit_brand']);
Route::post('update/{id}', [BrandController::class, 'update']);
Route::get('delete/{id}', [BrandController::class, 'delete']);

//Multi_pic
Route::get('multipic', [MultipicController::class, 'index'])->name('multipic');
Route::post('store/multipic', [MultipicController::class, 'store'])->name('store.multipic');

//Slider
// Route::get('slider', [SliderController::class, 'index'])->name('home.slider');
// Route::get('add/slider', [SliderController::class, 'create'])->name('add.slider');
// Route::post('store/slider', [SliderController::class, 'store'])->name('store.slider');

Route::resource('sliders', SliderController::class);
Route::get('index', [FrontController::class, 'index']);




//Dashboard
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {

    $users = User::all();
    // $users = DB::table('users')->get();
    return view('admin.index');
})->name('dashboard');

Route::get('logout', [BrandController::class, 'logout'])->name('logout');
