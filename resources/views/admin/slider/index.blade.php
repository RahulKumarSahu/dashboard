@extends('admin.admin_master')

@section('admin')

    <div class="py-12">
        <div class="container">
            <div class="row">
                <h4>Home Slider</h4>
                <a href="{{ route('sliders.create') }}"> <button class="btn btn-info">Add Slider</button></a>
                <br>
                <br>
                <div class="col-md-12">
                    <div class="card">
                        @if(session('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{ session('success') }}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                          </div>
                        @endif
                        <div class="card-header">
                            All Slider
                         </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                      <tr>
                                        <th scope="col">S No.</th>
                                        <th scope="col">Title</th>
                                        <th scope="col">Image</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Created At</th>
                                        <th scope="col">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        @php ($i = 1)
                                        @foreach ($sliders as $slider)
                                        <tr>
                                            <th scope="row">{{ $i++ }}</th>

                                            <td>{{ $slider->title }}</td>
                                            <td><img src="{{ asset($slider->image) }}" height="40px" width="70px" ></td>
                                            <td>{{ $slider->description }}</td>

                                            <td>
                                                @if ($slider->created_at == NULL)
                                                <span class="text-danger">No Date Set</span>
                                                @else
                                                    {{-- {{ $slider->created_at->diffForHumans()}} --}}
                                                    {{ Carbon\Carbon::parse($slider->created_at)->diffForHumans()}}
                                                    @endif
                                            </td>
                                            <td>
                                                 <a href="{{ route('sliders.show',$slider->id) }}" class="btn btn-warning">Show</a>

                                                 <a href="{{ route('sliders.edit',$slider->id) }}" class="btn btn-info">Edit</a>

                                                <form action="{{ route('sliders.destroy',$slider->id) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                <button type="submit" class="btn btn-danger" class="btn-white btn btn-xs">Delete</button>
                                                </form>

                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                  </table>
                                  {{ $sliders->links() }}
                            </div>
                    </div>
                </div>

            </div>
        </div>


    </div>
@endsection
