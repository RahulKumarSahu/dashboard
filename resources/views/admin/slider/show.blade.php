@extends('admin.admin_master')

@section('admin')

    <div class="py-12">
        <div class="container">
            <div class="row">

                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            Add Slider
                        </div>
                        <div class="card-body">
                            <form action="{{ route('sliders.show',$sliders->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf

                                <div class="mb-3">
                                  <label for="exampleInputEmail1" class="form-label">Title</label>
                                  <input type="text" class="form-control" name="title" value="{{ $sliders->title }}">
                                  @error('title')
                                  <span class="text-danger">{{ $message }}</span>
                                  @enderror
                                </div>

                                <div class="mb-3">
                                  <label for="exampleInputEmail1" class="form-label">Description</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description">
                                {{ $sliders->description }}
                            </textarea>

                                  @error('description')
                                  <span class="text-danger">{{ $message }}</span>
                                  @enderror
                                </div>


                                <div class="mb-3">
                                  <label for="exampleInputEmail1" class="form-label">Slider Image</label>
                                  <input type="file" class="form-control" name="image" value="{{ $sliders->image }}">
                                  @error('image')
                                  <span class="text-danger">{{ $message }}</span>
                                  @enderror
                                </div>
                                {{-- <button type="submit" class="btn btn-primary">Add Slider</button> --}}
                              </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>
@endsection
