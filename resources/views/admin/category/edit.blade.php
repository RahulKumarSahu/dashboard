
@extends('admin.admin_master')

@section('admin')
    <div class="py-12">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            Update Category
                        </div>
                        <div class="card-body">
                            <form action="{{ url('update/'.$categories->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="mb-3">
                                  <label for="category" class="form-label">Update Category Name</label>
                                  <input type="text" class="form-control" name="category_name" value="{{ $categories->category_name }}">

                                  @error('category_name')
                                  <span class="text-danger">{{ $message }}</span>
                                  @enderror

                                </div>
                                <button type="submit" class="btn btn-primary">Update Category</button>
                              </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

