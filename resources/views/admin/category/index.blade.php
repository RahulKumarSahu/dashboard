@extends('admin.admin_master')

@section('admin')

    <div class="py-12">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        @if(session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{ session('success') }}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                          </div>
                        @endif
                        <div class="card-header">
                            All Category
                         </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                      <tr>
                                        <th scope="col">S No.</th>
                                        <th scope="col">Category Name</th>
                                        <th scope="col">User</th>
                                        <th scope="col">Created At</th>
                                        <th scope="col">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        {{-- @php($i=1) --}}
                                        @foreach ($categories as $category)
                                        <tr>
                                            {{-- <th scope="row">{{ $i++ }}</th> --}}
                                            <th scope="row">{{ $categories->firstItem()+$loop->index }}</th>
                                            <td>{{ $category->category_name }}</td>
                                            <td>{{ $category->user->name }}</td>
                                            {{-- <td>{{ $category->name }}</td> --}}
                                            <td>
                                                @if ($category->created_at == NULL)
                                                <span class="text-danger">No Date Set</span>
                                                @else
                                                    {{-- {{ $category->created_at->diffForHumans()}} --}}
                                                    {{ Carbon\Carbon::parse($category->created_at)->diffForHumans()}}
                                                    @endif
                                            </td>
                                            <td>
                                                <a href="{{ url('editcate/'.$category->id) }}" class="btn btn-info">Edit</a>
                                                <a href="{{ url('softdelete/'.$category->id) }}"  class="btn btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                  </table>
                                  {{ $categories->links() }}
                            </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            Add Category
                        </div>
                        <div class="card-body">
                            <form action="{{ route('store.category') }}" method="POST">
                                @csrf
                                <div class="mb-3">
                                  <label for="exampleInputEmail1" class="form-label">Category Name</label>
                                  <input type="text" class="form-control" name="category_name">
                                  @error('category_name')
                                  <span class="text-danger">{{ $message }}</span>
                                  @enderror
                                </div>
                                <button type="submit" class="btn btn-primary">Add Category</button>
                              </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        {{-- trash --}}
        <br>

        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            Trash List
                         </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                      <tr>
                                        <th scope="col">S No.</th>
                                        <th scope="col">Category Name</th>
                                        <th scope="col">User</th>
                                        <th scope="col">Created At</th>
                                        <th scope="col">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($trachCat as $category)
                                        <tr>
                                            <th scope="row">{{ $categories->firstItem()+$loop->index }}</th>
                                            <td>{{ $category->category_name }}</td>
                                            <td>{{ $category->user->name }}</td>
                                            {{-- <td>{{ $category->name }}</td> --}}
                                            <td>
                                                @if ($category->created_at == NULL)
                                                <span class="text-danger">No Date Set</span>
                                                @else
                                                    {{-- {{ $category->created_at->diffForHumans()}} --}}
                                                    {{ Carbon\Carbon::parse($category->created_at)->diffForHumans()}}
                                                    @endif
                                            </td>
                                            <td>
                                                <a href="{{ url('restore/'.$category->id) }}" class="btn btn-info">Restore</a>
                                                <a href="{{ url('pdelete/'.$category->id) }}"  class="btn btn-danger"> P Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                  </table>
                                  {{ $trachCat->links()}}
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

