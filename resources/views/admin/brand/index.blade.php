@extends('admin.admin_master')

@section('admin')

    <div class="py-12">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        @if(session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{ session('success') }}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                          </div>
                        @endif
                        <div class="card-header">
                            All Brands
                         </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                      <tr>
                                        <th scope="col">S No.</th>
                                        <th scope="col">Brands Name</th>
                                        <th scope="col">Brands Name</th>
                                        <th scope="col">Created At</th>
                                        <th scope="col">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($brands as $brand)
                                        <tr>
                                            {{-- <th scope="row">{{ $brand->id }}</th> --}}
                                            <th scope="row">{{ $brands->firstItem()+$loop->index }}</th>

                                            <td>{{ $brand->brand_name }}</td>
                                            <td><img src="{{ asset($brand->brand_image) }}" height="40px" width="70px" ></td>
                                            {{-- <td>{{ $brand->name }}</td> --}}
                                            <td>
                                                @if ($brand->created_at == NULL)
                                                <span class="text-danger">No Date Set</span>
                                                @else
                                                    {{-- {{ $brand->created_at->diffForHumans()}} --}}
                                                    {{ Carbon\Carbon::parse($brand->created_at)->diffForHumans()}}
                                                    @endif
                                            </td>
                                            <td>
                                                <a href="{{ url('editbrand/'.$brand->id) }}" class="btn btn-info">Edit</a>
                                                <a href="{{ url('delete/'.$brand->id) }}" onclick="return confirm('Are you sure to delete')" class="btn btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                  </table>
                                  {{ $brands->links() }}
                            </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            Add brand
                        </div>
                        <div class="card-body">
                            <form action="{{ route('store.brand') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="mb-3">
                                  <label for="exampleInputEmail1" class="form-label">Brand Name</label>
                                  <input type="text" class="form-control" name="brand_name">
                                  @error('brand_name')
                                  <span class="text-danger">{{ $message }}</span>
                                  @enderror
                                </div>
                                <div class="mb-3">
                                  <label for="exampleInputEmail1" class="form-label">Brand Image</label>
                                  <input type="file" class="form-control" name="brand_image">
                                  @error('brand_image')
                                  <span class="text-danger">{{ $message }}</span>
                                  @enderror
                                </div>
                                <button type="submit" class="btn btn-primary">Add Brand</button>
                              </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>
@endsection
