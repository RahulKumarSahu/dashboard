<?php

namespace App\Http\Controllers;

use App\Models\Slider;

class FrontController extends Controller
{
    public function index()
    {
        $sliders = Slider::get();
        return view('layouts.body.slider', compact('sliders'));
    }

}
