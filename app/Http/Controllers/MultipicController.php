<?php

namespace App\Http\Controllers;

use App\Models\Multipic;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Image;

class MultipicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $images = Multipic::all();
        return view('admin.multipic.index', compact('images'));
    }

    public function store(Request $request)
    {
        $validated = $request->validate(
            [
                'image' => 'required',
            ],
        );
        $image = $request->file('image');
        foreach ($image as $multi_img) {

            $name_gen = hexdec(uniqid()) . '.' . $multi_img->getClientOriginalExtension();
            Image::make($multi_img)->resize(300, 200)->save('image/brand/' . $name_gen);
            $last_img = 'image/brand/' . $name_gen;

            Multipic::insert([

                'image' => $last_img,
                'created_at' => Carbon::now()
            ]);
        } //end foreach loop

        return Redirect()->back()->with('success', 'Brand Image Inserted Successfully');
    }
}