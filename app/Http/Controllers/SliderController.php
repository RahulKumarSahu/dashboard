<?php

namespace App\Http\Controllers;

use App\Http\Requests\SliderRequest;
use App\Models\Slider;
use Illuminate\Http\Request;

use Illuminate\Support\Carbon;
use Image;


class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::latest()->paginate();
        return view('admin.slider.index', compact('sliders'));
    }

    public function create()
    {
        return view('admin.slider.create');
    }

    public function store(SliderRequest $request)
    {

        $image = $request->file('image');
        $name_gen = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
        Image::make($image)->resize(1920, 1088)->save('image/slider/' . $name_gen);

        $img = 'image/slider/' . $name_gen;

        $slider = new Slider();
        $slider->title = $request->title;
        $slider->description = $request->description;
        $slider->image = $img;
        $slider->save();

        return Redirect()->route('sliders.index')->with('success', 'Slider Inserted Successfully');
    }
    public function show($id)
    {
        $sliders = Slider::find($id);
        return view('admin.slider.show', compact('sliders'));
    }
    public function edit($id)
    {
        $sliders = Slider::find($id);
        return view('admin.slider.edit', compact('sliders'));
    }

    public function update(Request $request ,$id)
    {
        // dd($request->all());
        $slider_id = $request->id;
        $old_image = $request->old_image;

        $image = $request->file('image');


        if ($image) {
            @unlink($old_image);
            $image = $request->file('image');
            $name_gen = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(870, 370)->save('image/slider/' . $name_gen);
            $save_url = 'image/slider/' . $name_gen;

            $slider = Slider::find($id);
            $slider->title = $request->title;
            $slider->description = $request->description;
            $slider->image = $save_url;

            $slider->save();
            return Redirect()->route('sliders.index')->with('success', 'Slider update with Image Successfully');

        } else {
            $slider = Slider::find($id);
            $slider->title = $request->title;
            $slider->description = $request->description;
            $slider->save();

            return Redirect()->route('sliders.index')->with('success', 'Slider update with Image Successfully');
        }


    }
    public function destroy($id)
    {
        $slider = Slider::find($id);
        $old_image = $slider->image;
        unlink($old_image);

        Slider::find($id)->delete();
        return Redirect()->back()->with('success', 'Slider Deleted Successfully');
    }
}
